# Wheater App
Simple weather app using [OpenWeatherMap](https://openweathermap.org/) API.

## How to use
1. Clone this repository
2. Create a file named `.env` in the root directory
3. Add the following line to the `.env` file
    ```bash
    REACT_APP_API_KEY=YOUR_API_KEY
    ```
4. Run `npm install` to install the dependencies
5. Run `npm start` to start the development server
6. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
7. Enjoy!


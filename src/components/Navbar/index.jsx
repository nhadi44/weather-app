import React from "react";

export const Navbar = () => {
  return (
    <>
      <nav className="navbar navbar-dark bg-primary">
        <div className="container">
          <a href="#" className="navbar-brand text-uppercase fw-semibold">
            Weather App
          </a>
        </div>
      </nav>
    </>
  );
};

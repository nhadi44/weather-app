import React, { useState } from "react";
import moment from "moment";

export const Card = (props) => {
  const [date, setDate] = useState(moment().format("dddd, MMMM D YYYY"));
  const [time, setTime] = useState(moment().format("h:mm:ss a"));
  return (
    <>
      <div className="card">
        <div className="card-header bg-primary">
          <h5 className="card-title text-white">{props.title}</h5>
        </div>
        <div className="card-body p-5">
          {props.weather.map((item, index) => (
            <div
              className="d-flex justify-content-between align-items-center mb-5"
              key={index}
            >
              <div className="card-content-left">
                <h2 className="text-capitalize m-0">{item.main}</h2>
                <span className="text-muted text-capitalize">
                  {item.description}
                </span>
              </div>
              <div className="card-content-right">
                <div className="d-flex align-items-center justify-content-end">
                  <h2 className="text-muted m-0">{props.temp} &#8451;</h2>
                  <img
                    src={`https://openweathermap.org/img/wn/${item.icon}.png`}
                    alt=""
                  />
                </div>
                <div className="d-flex flex-column align-items-end">
                  <span>{date}</span>
                  <span>{time}</span>
                </div>
              </div>
            </div>
          ))}
          <div className="pt-5 d-flex justify-content-between">
            <div className="temp_min d-flex flex-column align-items-center">
              <small className="text-muted">Temp min</small>
              <span>{props.tempMin} &#8451;</span>
            </div>
            <div className="temp_max d-flex flex-column align-items-center">
              <small className="text-muted">Temp max</small>
              <span>{props.tempMax} &#8451;</span>
            </div>
            <div className="wind d-flex flex-column align-items-center">
              <small className="text-muted">Wind</small>
              <span>{props.wind} km/h</span>
            </div>
            <div className="humidity d-flex flex-column align-items-center">
              <small className="text-muted">Humidity</small>
              <span>{props.humidity}%</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

import React, { useEffect, useState } from "react";
import { Card, Navbar } from "./components";

export const App = () => {
  const [lat, setLat] = useState(null);
  const [lon, setLon] = useState(null);
  const [location, setLocation] = useState("");
  const [weather, setWeather] = useState([]);
  const [country, setCountry] = useState("");
  const [loading, setLoading] = useState(true);
  const [city, setCity] = useState("");

  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        setLat(position.coords.latitude);
        setLon(position.coords.longitude);
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }, [lat, lon]);

  const getWeatherByCoor = async () => {
    setLoading(true);
    const url = new URL(`${process.env.REACT_APP_API_URL}/weather`);
    url.searchParams.append("lat", lat);
    url.searchParams.append("lon", lon);
    url.searchParams.append("appid", process.env.REACT_APP_API_KEY);
    url.searchParams.append("units", "metric");

    const response = await fetch(url);
    const data = await response.json();
    setLoading(false);
    setWeather(data);
    setLocation(data.name);
    setCountry(data.sys.country);
  };

  const getWeatherByCity = async (city) => {
    setLoading(true);
    const url = new URL(`${process.env.REACT_APP_API_URL}/weather`);
    url.searchParams.append("q", city);
    url.searchParams.append("appid", process.env.REACT_APP_API_KEY);
    url.searchParams.append("units", "metric");

    const response = await fetch(url);
    const data = await response.json();
    setLoading(false);
    setWeather(data);
    setLocation(data.name);
    setCountry(data.sys.country);
    setCity("");
  };

  useEffect(() => {
    if (lat && lon) {
      getWeatherByCoor();
    }
  }, [lat, lon]);

  return (
    <>
      <header className="mb-5">
        <Navbar />
        <section className="container my-4">
          <h3 className="text-center">
            {location}, {country}
          </h3>
          <div className="row d-flex justify-content-center">
            <div className="col-md-4">
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  getWeatherByCity(city);
                }}
              >
                <div className="d-flex align-items-center gap-2">
                  <input
                    type="text"
                    placeholder="Search City"
                    className="form-control"
                    onChange={(e) => setCity(e.target.value)}
                    value={city}
                  />
                  <button
                    className="btn btn-primary"
                    onClick={() => getWeatherByCity(city)}
                  >
                    <i class="bi bi-search"></i>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </section>
      </header>
      <main className="container">
        {loading ? (
          <>
            <div className="row w-100 d-flex justify-content-center">
              <span>Loading...</span>
            </div>
          </>
        ) : (
          <div className="row w-100 d-flex justify-content-center">
            <div className="col-md-8">
              <Card
                title={weather.name}
                weather={weather.weather}
                temp={weather.main.temp}
                tempMin={weather.main.temp_min}
                tempMax={weather.main.temp_max}
                wind={weather.wind.speed}
                humidity={weather.main.humidity}
              />
            </div>
          </div>
        )}
      </main>
    </>
  );
};
